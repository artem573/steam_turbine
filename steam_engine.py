import cantera as ct
import numpy as np
import sys
import csv

class State():
    def __init__(self):
        self.mix_KS = None
        self.alpha = None
        self.steam = None
        self.mix_SM = None
        self.mix_after_turb = None
        self.mix_out = None
        


T = 300.0
T_f = T
T_o = T
p = 15 * ct.one_atm

T_C_to_K = 273.0
T_steam = 200.0 + T_C_to_K # [K]

T_KS_goal = 1027.0 + T_C_to_K # [K]
T_SM_goal = 600.0 + T_C_to_K # [K]
T_after_turb = 400.0 + T_C_to_K # [K]
T_out = 90.0 + T_C_to_K # [K]

T_tol = 2.0 # [K]


gas_st = ct.Solution('gri30.cti')
gas_st.TPX = T, p, 'CH4:1, O2:2, N2:7.52'
Y_f_st = gas_st.Y[gas_st.species_index('CH4')]
mo_mf_st = (1.0 - Y_f_st) / Y_f_st

states = []

mass_f = 1.0 # [kg]
mass_o_st = mo_mf_st * mass_f

fuel_comp = 'CH4:1'
gas_f = ct.Solution('gri30.cti')
gas_f.TPX = T_f, p, fuel_comp
fuel = ct.Quantity(gas_f, mass=mass_f)

oxydizer_comp = 'O2:1.0, N2:3.76'
gas_o = ct.Solution('gri30.cti')
gas_o.TPX = T_o, p, oxydizer_comp

it_max = 1000

mass_o_min = 1e-3 * mass_o_st
mass_o_max = mass_o_st
for it in range(0, it_max):
    mass_o = 0.5 * (mass_o_min + mass_o_max)

    oxydizer = ct.Quantity(gas_o, mass=mass_o)

    mix_KS = fuel + oxydizer
    mix_KS.equilibrate('HP')

    if np.abs(mix_KS.T - T_KS_goal) < T_tol:
        states.append(State())
        states[-1].mix_KS = mix_KS
        states[-1].alpha = (mass_o / mass_f) / mo_mf_st
        break

    if mix_KS.T < T_KS_goal: mass_o_min = mass_o
    else: mass_o_max = mass_o
    
mass_o_min = mass_o_st
mass_o_max = 1e+3 * mass_o_st

for it in range(0, it_max):
    mass_o = 0.5 * (mass_o_min + mass_o_max)

    oxydizer = ct.Quantity(gas_o, mass=mass_o)

    mix_KS = fuel + oxydizer
    mix_KS.equilibrate('HP')

    if np.abs(mix_KS.T - T_KS_goal) < T_tol:
        states.append(State())
        states[-1].mix_KS = mix_KS
        states[-1].alpha = (mass_o / mass_f) / mo_mf_st
        break
    if mix_KS.T > T_KS_goal: mass_o_min = mass_o
    else: mass_o_max = mass_o
    
    
steam_comp = 'H2O:1.0'
gas_steam = ct.Solution('gri30.cti')
gas_steam.TPX = T_steam, p, steam_comp
mass_steam = 1.0 # [kg]
steam = ct.Quantity(gas_steam, mass=mass_steam)


for state in states:
    state.steam = steam

    mass_PS_min = 1e-3 * steam.mass
    mass_PS_max = 1e+3 * steam.mass

    for it in range(0, it_max):
        mass_PS = 0.5 * (mass_PS_min + mass_PS_max)
        state.mix_KS.mass = mass_PS

        mix_SM = state.mix_KS + steam
        mix_SM.equilibrate('HP')

        if np.abs(mix_SM.T - T_SM_goal) < T_tol:
            state.mix_SM = mix_SM
            break

        if mix_SM.T < T_SM_goal: mass_PS_min = mass_PS
        else: mass_PS_max = mass_PS
        

    # state.after_turb = state.mix_SM
    # state.after_turb.T = T_after_turb

    # state.out = state.after_turb
    # state.out.Y[state.out.species_index('H2O')] = 0.0
    # state.out.T = T_out

    print('state 1:', 'T:', state.steam.T, 'Cp:', state.steam.cp_mass, 'm:', state.steam.mass)
    print('state 2:', 'T:', state.mix_KS.T, 'Cp:', state.mix_KS.cp_mass, 'm:', state.mix_KS.mass, 'alpha:', state.alpha)
    print('state 3:', 'T:', state.mix_SM.T, 'Cp:', state.mix_SM.cp_mass, 'm:', state.mix_SM.mass)
    # print('state 4:', 'T:', state.after_turb.T, 'Cp:', state.after_turb.cp_mass, 'm:', state.after_turb.mass)
    # print('state 5:', 'T:', state.out.T, 'Cp:', state.out.cp_mass, 'm:', state.out.mass)

    
